//********************************************************************
//File:         Problem1.java       
//Author:       [Yishu Xin]
//Date:         [Nov, 15 2017]
//Course:       CPS100
//
//Problem Statement:
//Write a program that creates a histogram that allows you to visually inspect
//the score distribution of a set of students. The program should read a file and
//display how many numbers of integers that are in the range 0 to 
//100 inclusive; then produce a chart similar to the one below that indicates
//how many students scored in the range 91 to 100, 81 to 90 and so on. 
//Print stars for each student in that range of the file.

//Inputs: file  
//Outputs: distribution of numbers displayed by stars
// 
//********************************************************************



import java.io.*;
import java.util.*;



public class Histogram
{
  public static void main(String[] args) throws IOException
  {

    int[] counts = new int[101];
    int increasment = 0;


    Scanner fileScanner = new Scanner(new File("realdata.txt"));


    while (fileScanner.hasNextLine())
    {
      int nums = fileScanner.nextInt();
      counts[nums]++;
    }

    for (int count = 1; count <= 100; count += 10)
    {
      System.out.print(count + " to " + (increasment += 10) + " ");
      
      for (int i = count; i <= increasment; i++)
      {
        while (counts[i] >0 )
        {
          System.out.print("*");
          counts[i]--;
        }
        
      }
      
      System.out.println();
      
    }

  }

}

